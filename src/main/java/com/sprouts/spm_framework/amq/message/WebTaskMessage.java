package com.sprouts.spm_framework.amq.message;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Session;

import com.sprouts.spm_framework.enums.MonitorType;

public class WebTaskMessage extends BaseMessage {

    private String destination;
    private String monitorName;
    private MonitorType type;
    private int port;
    private int frequency;

    @Override
    public MapMessage generateMessage(Session session) {
        MapMessage msg = null;
        try {
            msg = session.createMapMessage();
            msg.setString("destination", destination);
            msg.setString("type", type.type);
            msg.setString("monitorName", monitorName);
            msg.setInt("port", port);
            msg.setInt("frequency", frequency);
        } catch (JMSException e) {
            logger.error("generate WebTaskMessage error:", e);
        }
        return msg;
    }



    @Override
    public String toString() {
        return "WebTaskMessage [destination=" + destination + ", monitorName=" + monitorName
                + ", type=" + type + ", port=" + port + ", frequency=" + frequency + "]";
    }



    @Override
    public BaseMessage parseMapMsgToBaseMsg(MapMessage msg) {
        WebTaskMessage webMessage = new WebTaskMessage();
        try {
            webMessage.destination = msg.getString("destination");
            webMessage.type = MonitorType.valueOf(msg.getString("type"));
            webMessage.port = msg.getInt("port");
            webMessage.monitorName = msg.getString("monitorName");
            webMessage.frequency = msg.getInt("frequency");
        } catch (JMSException e) {
            logger.error("error in parse map msg to base appser msg:\n", e);
        }
        return webMessage;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getMonitorName() {
        return monitorName;
    }

    public void setMonitorName(String monitorName) {
        this.monitorName = monitorName;
    }

    public MonitorType getType() {
        return type;
    }

    public void setType(MonitorType type) {
        this.type = type;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }



    public int getFrequency() {
        return frequency;
    }



    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

}
