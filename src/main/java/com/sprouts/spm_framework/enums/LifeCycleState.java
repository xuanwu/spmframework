package com.sprouts.spm_framework.enums;

public enum LifeCycleState {
    STARTED(0), NOTSTARTED(1), DELETE(2);

    public int state;

    private LifeCycleState(int state) {
        this.state = state;
    }
}
