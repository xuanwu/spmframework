package com.sprouts.spm_framework.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import sun.misc.BASE64Encoder;

public class HTTPUtils {

    private static Logger logger = new Logger();

    /**
     * doGet方法
     * 
     * @param url
     * @param objs
     * @return 响应结果的字符串
     * @throws Exception
     */
    public static String doGet(String url, String type) throws Exception {
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader reader = null;
        StringBuffer resultBuffer = new StringBuffer();


        if (url != null) {
            URL localURL = new URL(url);
            URLConnection connection = localURL.openConnection();
            HttpURLConnection httpURLConnection = (HttpURLConnection) connection;

            // 如果是应用服务器监控请求，则添加一个权限
            if (type != null && type.equalsIgnoreCase("appserver")) {
                String password = "tomcat:tomcat"; // manager角色的用户
                String encodedPassword = new BASE64Encoder().encode(password.getBytes());
                httpURLConnection.setRequestProperty("Authorization", "Basic " + encodedPassword);
            }

            httpURLConnection.setRequestProperty("Accept-Charset", "utf-8");
            httpURLConnection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");


            String tempLine = null;

            if (httpURLConnection.getResponseCode() >= 300) {
                throw new Exception("HTTP Request is not success, Response code is "
                        + httpURLConnection.getResponseCode());
            }

            try {
                inputStream = httpURLConnection.getInputStream();
                inputStreamReader = new InputStreamReader(inputStream);
                reader = new BufferedReader(inputStreamReader);

                while ((tempLine = reader.readLine()) != null) {
                    resultBuffer.append(tempLine);
                }

            } finally {

                if (reader != null) {
                    reader.close();
                }

                if (inputStreamReader != null) {
                    inputStreamReader.close();
                }

                if (inputStream != null) {
                    inputStream.close();
                }

            }
        } else {
            logger.warn("doGet error: The url is null!");
        }

        return resultBuffer.toString();
    }

    /**
     * doPost方法
     * 
     * @param url
     * @param objs
     * @return 响应结果的字符串
     * @throws Exception
     */
    public static String doPost(String url, Object... objs) throws Exception {
        String parameterData = "";
        for (int i = 0; i < objs.length; i++) {
            parameterData += (i % 2 == 0) ? (String) objs[i] + "=" : (String) objs[i];
            parameterData += ((i % 2 == 1) && (i + 1 < objs.length)) ? "&" : "";
        }

        OutputStream outputStream = null;
        OutputStreamWriter outputStreamWriter = null;
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader reader = null;
        StringBuffer resultBuffer = new StringBuffer();

        if (url != null) {
            URL localURL = new URL(url);
            URLConnection connection = localURL.openConnection();
            HttpURLConnection httpURLConnection = (HttpURLConnection) connection;

            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Accept-Charset", "utf-8");
            httpURLConnection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("Content-Length",
                    String.valueOf(parameterData.length()));


            String tempLine = null;

            try {
                outputStream = httpURLConnection.getOutputStream();
                outputStreamWriter = new OutputStreamWriter(outputStream);

                outputStreamWriter.write(parameterData.toString());
                outputStreamWriter.flush();

                if (httpURLConnection.getResponseCode() >= 300) {
                    throw new Exception("HTTP Request is not success, Response code is "
                            + httpURLConnection.getResponseCode());
                }

                inputStream = httpURLConnection.getInputStream();
                inputStreamReader = new InputStreamReader(inputStream);
                reader = new BufferedReader(inputStreamReader);

                while ((tempLine = reader.readLine()) != null) {
                    resultBuffer.append(tempLine);
                }

            } finally {

                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }

                if (reader != null) {
                    reader.close();
                }

                if (inputStreamReader != null) {
                    inputStreamReader.close();
                }

                if (inputStream != null) {
                    inputStream.close();
                }
            }
        } else {
            logger.warn("doPost error : The url is null!");
        }


        return resultBuffer.toString();
    }
}
