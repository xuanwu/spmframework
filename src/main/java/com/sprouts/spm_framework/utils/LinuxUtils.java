package com.sprouts.spm_framework.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class LinuxUtils {

    private static Logger logger = new Logger();

    /**
     * 执行相应的命令，获得终端输出
     * 
     * @param commandStr 命令
     * @return
     */

    public static ArrayList<String> execCommand(String commandStr) {

        ArrayList<String> data = new ArrayList<String>();
        List<String> shellString = new ArrayList<String>();
        shellString.add("sh");
        shellString.add("-c");
        shellString.add(commandStr);

        String line = null;

        try {
            ProcessBuilder builder = new ProcessBuilder(shellString);
            Process process = builder.start();
            InputStream inputStream = process.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            while ((line = reader.readLine()) != null) {
                data.add(line);
            }

            reader.close();
            inputStream.close();
        } catch (Exception e) {
            logger.error("Exception in method execCommand:", e);
        }
        return data;
    }

    /**
     * 执行相应的命令，获得制定行数的终端输出
     * 
     * @param commandStr 命令
     * @param lines 指定行数
     * @return
     */

    public static ArrayList<String> execCommand(String commandStr, int lines) {

        ArrayList<String> data = new ArrayList<String>();
        List<String> shellString = new ArrayList<String>();
        shellString.add("sh");
        shellString.add("-c");
        shellString.add(commandStr);

        String line = null;
        int count = 0;
        try {
            ProcessBuilder builder = new ProcessBuilder(shellString);
            Process process = builder.start();
            InputStream inputStream = process.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            while ((line = reader.readLine()) != null && count <= lines) {
                data.add(line);
                count++;
            }

            reader.close();
            inputStream.close();
        } catch (Exception e) {
            logger.error("Exception in method execCommand:", e);
        }

        return data;
    }

    /**
     * 执行需要等待完成才能继续的任务
     * 
     * @param commandStr
     */
    public static void execWaitCommand(String commandStr) {
        List<String> shellString = new ArrayList<String>();
        shellString.add("sh");
        shellString.add("-c");
        shellString.add(commandStr);

        try {
            ProcessBuilder builder = new ProcessBuilder(shellString);
            Process process = builder.start();
            process.waitFor();
        } catch (InterruptedException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        }
    }

}
